#! /usr/bin/python3
# encoding:utf8
# 👾

"""
Script-bricolage de résolution du taquin à l'aide d'A*
L'état du jeu est représenté par un tuple et la case
vide est représentée par un 0

Exemple :

+---+---+---+
| 1 | 2 | 3 |
+---+---+---+
| 4 | 5 | 6 |  ------>  (1, 2, 3, 4, 5, 6, 7, 8, 0)
+---+---+---+
| 7 | 8 |   |
+---+---+---+
"""

import time
from heapq import *
import random

def build_path(start, target, parent_list):
    """
    Fonction de reconstruction d'une solution pas-à-pas
    à partir de la liste des états parents
    """
    if target not in parent_list:
        return None
    else:
        path = [target]
        s = target
        while s != start:
            s = parent_list[s]
            path.append(s)
        return path[::-1]

def gen_h(target, nl, nc):
    """
    Générateur d'une heuristique (basée sur la distance de Manhattan)
    Exemple :                   +---------------------------------------+
                                | Nombre de cases horiz. et vert.       |
         +---+---+---+        / | séparant le "8" de sa position finale |
         | 8 | 2 | 3 |       |  +---------------------------------------+
         +---+---+---+       |
    h(   | 4 | 5 | 6 |   ) = 3 + 0 + 0 + 0 + 0 + 0 + 0 + 4
         +---+---+---+                                   |
         | 7 |   | 1 |                                  /
         +---+---+---+          +---------------------------------------+
                                | Nombre de cases horiz. et vert.       |
                                | séparant le "1" de sa position finale |
                                +---------------------------------------+
    """
    pos_f = {i:(int(target.index(i)/nl),target.index(i)%nl) for i in range(nl*nc)}
    def h(T):
        return sum([abs(int(i/nl)-pos_f[T[i]][0])+abs(i%nl-pos_f[T[i]][1]) for i in range(nl*nc) if T[i] != 0])
    return h

def gen_dev(nl, nc):
    """
    Générateur d'une fonction fournissants les développements possibles d'un état du jeu
    (retourne les états "enfants")
    L'idée est de modéliser un coup possible comme un déplacement de la case vide

         +---+---+---+       +---+---+---+
         | 8 | 2 | 3 |       | 8 | 2 | 3 |
         +---+---+---+       +---+---+---+
         | 4 | 5 | 6 | --->  | 4 | 5 | 6 |
         +---+---+---+       +---+---+---+
         | 7 | --> 1 |       | 7 | 1 |   |
         +---+---+---+       +---+---+---+

    Un coup possible est donc un déplacement de cette case parmi Nord Sud Est et Ouest
    dépendant de sa position.
    """
    def dev(T):
        move_pos=dict()
        pos0 = T.index(0)
        child_states = []
        for m in [p2 for p2 in [pos0-1,pos0+1,pos0-nc,pos0+nc] if (int(pos0/nc)==int(p2/nc) or pos0%nc==p2%nc) and p2>=0 and p2<nc*nl] :
            child = list(T)
            child[pos0] = T[m]
            child[m] = T[pos0]
            child_states.append(tuple(child))
        return child_states
    return dev

def shuffle(T, nl, nc, n):
    """
    Fonction de mélange aléatoire d'un état de jeu
    en respectant les mouvements possibles
    (utilisé pour la résolution du taquin à 5 cases)
    Il s'agit donc d'un mélange par déplacements aléatoires
    de la case vide.
    """
    for _ in range(n):
        move_pos=dict()
        pos0 = T.index(0)
        child_states = []
        for m in [p2 for p2 in [pos0-1,pos0+1,pos0-nc,pos0+nc] if (int(pos0/nc)==int(p2/nc) or pos0%nc==p2%nc) and p2>=0 and p2<nc*nl] :
            child = list(T)
            child[pos0] = T[m]
            child[m] = T[pos0]
            child_states.append(tuple(child))
        T = child_states[random.randint(0,len(child_states)-1)]
    return T

def solver_taquin(start, target, nl, nc):
    """
    Résolution du taquin avec A*
    Implémentation retenue :
        - avec vclose (pour éventuellement filtrer les sorties)
        - avec 'heapq'
        - sans suppression dans vopen lors d'une maj
    """
    cost_fromstart = dict()   # Coût des chemins
    parent = dict()           # Etats parents
    vopen = []                # Etats ouverts
    vclose = set()            # Etats fermés
    eval = dict()             # Fonction d'évaluation
    dev = gen_dev(nl, nc)     # Fonction de développement
    h = gen_h(target, nl, nc) # Fonction heuristique

    # Initialisation
    cost_fromstart[start] = 0                      # Le cout du pcc de start à start vaut 0
    eval[start] = cost_fromstart[start] + h(start) # f(start) = h(start), du coup
    heappush(vopen,(eval[start],start))            # On met start dans la file à priorité
    parent[start] = None                           # Le sommet start n'a pas d'antécédent

    # Boucle principale
    while vopen:
        p, current_state = heappop(vopen)
        vclose.add(current_state)
        if current_state == target:
            break
        for child in dev(current_state):
            c = cost_fromstart[current_state] + 1
            if child not in cost_fromstart or c < cost_fromstart[child]:
                cost_fromstart[child] = c
                eval[child] = c + h(child)
                parent[child] = current_state
                heappush(vopen,(eval[child],child))
                if child in vclose:
                    vclose.remove(child)

    # Retour du résultat
    return build_path(start, target, parent)

def pprint(state, nl, nc):
    """
    Affichage civilisé d'un état du jeu
    en ascii + ansi-escape-codes (pour la couleur)
    """
    width = len(str(max(state)))
    txt = u"\u001b[36;1m\n+"
    c = 0
    txt += ("-"*(width+2) + "+")*nc + "\n"
    for i in range(nl):
        txt += "|"
        for j in range(nc):
            txt += " "+ (str(state[c]) if state[c]!=0 else " ").rjust(width) +" |"
            c += 1
        txt += "\n+"+ ("-"*(width+2) + "+")*nc + "\n"
    txt += "\u001b[0m"
    print(txt,flush=True)
    return None

def refresh_screen():
    """
    Vide le terminal et place le curseur en haut à gauche
    """
    print('\033[2J')
    print('\033[0;0H')
    return None

def print_header():
    """
    Affichage de l'en-tête du programme
    """
    print(u"\u001b[32;1m"+
           " _____                 _                          _       \n"+
           "|_   _|_ _  __ _ _   _(_)_ __   __   _____       / \  _\/_\n"+
           "  | |/ _` |/ _` | | | | | '_ \  \ \ / / __|     / _ \  /\ \n"+
           "  | | ( | | ( | | |_| | | | | |  \ V /\__ \_   / ___ \    \n"+
           "  |_|\__,_|\__, |\__,_|_|_| |_|   \_/ |___(_) /_/   \_\   \n"+
           "              |_|                                         \n"+
           "\u001b[0m")
    return None

def waiting_return():
    """
    Fonction d'attente d'un <return> de l'utilisateur
    """
    _ = input("\033[5m\u001b[90m\u001b[47mAppuyer sur <Entrée> pour continuer\u001b[0m")
    print(u"\u001b[1A\r                                   \r")
    print(u"\u001b[2A\r")
    return None

# +---------------+
# |    M A I N    |
# +---------------+
if __name__=="__main__":

    refresh_screen()
    print_header()
    waiting_return()

    # --------------------------
    # Résolution d'un taquin 3x3
    # --------------------------

    print("\u001b[45mRésolution d'un taquin 3x3\u001b[0m\n")
    print("- État initial :")
    start = (8,1,3,
             4,0,2,
             7,6,5)
    pprint(start,3,3)
    t0 = time.time()
    path = solver_taquin(start,
                         (1,2,3,
                          4,5,6,
                          7,8,0),3,3)
    t1 = time.time()
    print(f"- Solution à {len(path)} coups, obtenue en {round(t1-t0,3)} secondes :")
    # Affichage de la solution
    if path is not None:
        for s in path:
            pprint(s, 3, 3)
            time.sleep(0.5)
            print(u"\u001b[10A",flush=True)
        print(u"\u001b[10B")

    waiting_return()


    # --------------------------
    # Résolution d'un taquin 4x4
    # --------------------------

    refresh_screen()
    print_header()
    waiting_return()


    print("\u001b[45mRésolution d'un taquin 4x4\u001b[0m\n")

    print("- État initial :")
    start = (6, 8,11, 7,
             9, 1,12,10,
             4,13, 2,15,
             3, 5,14, 0)
    pprint(start,4,4)

    t0 = time.time()
    path = solver_taquin(start,( 1, 2, 3, 4,
                                 5, 6, 7, 8,
                                 9,10,11,12,
                                13,14,15, 0 ), 4, 4)
    t1 = time.time()

    print(f"- Solution en {len(path)} coups, obtenue en {round(t1-t0,3)} secondes :")

    if path is not None:
        for s in path:
            pprint(s, 4 , 4)
            time.sleep(0.5)
            print(u"\u001b[12A",flush=True)
        print(u"\u001b[12B")

    waiting_return()

    # --------------------------
    # Résolution d'un taquin 5x5
    # --------------------------

    refresh_screen()
    print_header()
    waiting_return()

    print("\u001b[45mRésolution d'un taquin 5x5\u001b[0m\n")
    print("- État initial :")
    rand_start = shuffle(( 1, 2, 3, 4, 5,
                           6, 7, 8, 9,10,
                          11,12,13,14,15,
                          16,17,18,19,20,
                          21,22,23,24, 0 ), 5, 5, 120)
    pprint(rand_start,5,5)
    t0 = time.time()
    path = solver_taquin(rand_start,
                         ( 1, 2, 3, 4, 5,
                           6, 7, 8, 9,10,
                          11,12,13,14,15,
                          16,17,18,19,20,
                          21,22,23,24, 0 ), 5, 5)
    t1 = time.time()
    print(f"- Solution en {len(path)} coups, obtenue en {round(t1-t0,3)} secondes :")
    if path is not None:
        for s in path:
            pprint(s, 5 , 5)
            time.sleep(0.5)
            print(u"\u001b[14A",flush=True)
        print(u"\u001b[14B")

# +---------------+
# |  🤖 E N D  👾  |
# +---------------+
