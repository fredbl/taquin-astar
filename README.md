# taquin-astar

Résolution du taquin à l'aide d'A\* (en python). Petit script-bricolage-pédagogique de résolution du jeu du taquin avec A* (et une heuristique « de Manhattan »)

- `taquin-s=astar.py` : script tout-en-un
- `TaquinAstarInMyTerm.mp4` : vidéo de démonstration
